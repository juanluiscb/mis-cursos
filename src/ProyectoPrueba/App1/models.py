from django.db import models

class Alumno(models.Model):
    al_matricula = models.CharField(max_length=8)
    al_nombre = models.CharField(max_length=50)
    al_paterno = models.CharField(max_length=50)
    al_materno = models.CharField(max_length=50)
    al_activo = models.BooleanField(default=True)


