# Nivel Básico
 
 - Creando Entorno Virtual [env](Basic.md#entorno-virtual)
 - [Activación](Basic.md#activando-entorno-virtual) del Entorno Virtual 
 - Instalando [Django](Basic.md#entorno-virtual)
 - Creando un [Proyecto](Basic.md#creando-proyecto)
 - [Escructura](Basic.md#escructura-del-proyecto) del Proyecto
 - Creando una [Aplicación](Basic.md#creando-una-app) para el proyecto
 - Correr [Hola Mund](Basic.md#corriendo-hola-mundo-en-django) o en Django

## Entorno Virtual

El uso de Entornos Virtuales es recomendado, para no dañar nuestro entorno global, y para poder utilizar distintas versiones de paquetes.

Se suele nombrar a un entorno virtual como **venv**, pero esto es completamente a gusto de cada quien.

Una vez instalado ___virtualenv___ podemos crear un entorno virtual, dentro de la carpeta de trabajo.

Crearemos una carpeta de trabajo en el escritorio.

    #Cambiamos de directorio al escritorio
    cd Desktop
    #Creamos nuestra carpeta de trabajo
    mkdir django-basico
    #cambianos de directorio a nuestra carpeta de trabajo
    cd django-basico

    #creamos nuestro entorno virtual
    virtualenv venv

![venv](assets/venv.png)

## Activando Entorno Virtual
Una vez creado en venv, debemos activarlo. Se debe estar en la raiz de nuestra carpeta de trabajo

    source venv/bin/activate

![activate-venv](assets/venv_activate.png)

Dependiendo de la terminal y la configuración que se tenga, es como deberá aparecer, para este caso estoy usando, la terminal tilix con zsh.Pero en bash se vería de la siguiente manera
![venv-bash](assets/venv_activate_bash.png)

Revisar la documentación para la activación en Windows

## Instalación de Django

Con el entorno virtual activado y utilizando el gestor de paquetes de Python pip, instalamos Django.

    pip install django

![install-django](assets/install_django.png)

La versión de django al momento de crear este documento es la 4.1.3, si se desea una anterior sería de la siguiente forma:

    #instalamos la version 3.2.16
    pip install django==3.2.16

Las versiones de los paquetes los podemos encontrar el la [web][ref1]

Para comprobar que se instalalara correctamente:

    pip list

![pip-list](assets/pip-list.png)

## Creando Proyecto

Una vez instalado el paquete de django, podemos crear proyectos y agregar aplicaciones a dicho proyecto

    django-admin startproject ProyectoPrueba

![startproject](assets/startproject.png)

Se creará una carpeta con nuestro proyecto creado.

## Escructura del proyecto

Nos cambiamos a la carpeta del proyecto y podemos ver la siguiente estructura, opcionalmente se puede usar el comando tree o simplemente ls.

1. Carpeta General del Proyecto
2. Archivo principal del proyecto django
3. Carpeta de configuración del proyecto
4. Archivo de Configuración del proyecto
5. Archivo para manejo de urls

Los únicos archivos que vamos a editar son: 4 y 5.

![estructura-project](assets/estructura-project.png)


## Creando una App
Una vez dentro de nuestro proyecto, podemos crear aplicaciones para el mismo.

    python manage.py startapp App1


La estructura básica de la aplicación:

1. Carpeta que nos indica el nombre de la Aplicación y su contenido
2. En este archivo declararemos los modelos de nuestra aplicación
3. Este archivo recibe y responte peticiones Http

![estruct-app](assets/escructura-app.png)

Muy importante, esto hay que hacerlo manualmente, cuando utilizamos un IDE como PyCharm lo hace por nosotros con la primera aplicación, pero debemos relacionar la aplicación con el proyecto, y eso lo hacemos en el archivo settings.py

![add-app-settings](assets/app-add-settings.png)

## Corriendo Hola Mundo en Django

Como en todo lenguaje existe un simple Hola Mundo, en django no es la excepción.

Para crear nuestro hola mundo solo trabajaremos en 2 archivos:
1. Escribimos el código que muestra el mensaje
2. Enlazamos desde la url hacia el medodo que muestra el mensaje

![Hola-Mundo](assets/hola-mundo.png)

- Achivo Views.py
    Creamos el método para el hola mundo

![hola-code](assets/views-holamundo.png)

- Archivo urls.py
1. Importamos nuestro metodo 
2. lo agregamos a las rutas de django
3. le indicamos que en la ruta principal debe cargar el hola mundo


![urls-hola](assets/url-holamundo.png)

Ahora solo resta correr nuestro proyecto

    python manage.py runserver

![runserver-hola](assets/runserver-holamundo.png)

visitamos esa url en cualquier navegador de nuestra computadora

![nav-hola](assets/navegador-holamundo.png)


[ref1]: https://pypi.org/project/Django/#history