# Modelos de Django
- [Introducción](Models.md#introducción)
- Dónde escribir mis [modelos](Models.md#dónde-escribir-mis-modelos-en-django) de Django
- [Creación de un Modelo](Models.md#creación-de-un-modelo-de-django)

## Introducción
Como ya hemos hablado, el modelo, es quien se encarga de la pertinencia de nuestra información, además de darnos las conexiones necesarias para interactuar con la Base de datos.

Por default, django utiliza una base de datos local, llamada [SQLite3][sqlite3], pero la documentación nos brinda [información][doc-db] para lograr conectarnos a una base de datos como MySQL, Postgress, MariaDB, etc.

En este ejemplo utilizaremos la DB por default, pero los modelos se pueden migrar a cualquier otra conexión.

## Dónde escribir mis Modelos en Django

Los Modelos estarán dentro de la Aplicación, en un archivo llamado models.py, y notaremos que por default, viene una línea en ese archivo, haciendo referencia a la interfaz models, indica que cada modelo que querramos que sea parte de Django, debe ser de tipo models.Model, lo veremos más adelante con un ejemplo.

## Creación de un Modelo de Django

Un modelo es una clase de Python, con ciertas peculiaridades, por ejemplo solo declaramos los artributos y los asignamos a un tipo específico de tipo de datos de Django.

Crearemos el modelo Alumno, con sus respectivos atributos.
ver [aquí](src/ProyectoPrueba/App1/models.py)



[sqlite3]: https://www.sqlite.org/index.html
[doc-db]: https://docs.djangoproject.com/en/4.1/ref/settings/#databases