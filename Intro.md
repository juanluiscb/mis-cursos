# Introducción

- Qué es un [FrameWork](Intro.md#framework)
- Qué es [Django](Intro.md#django)
- [MVC - MVT](Intro.md#mvc---mvt)
- Antes de comenzar, que [necesitamos](Intro.md#antes-de-comenzar)

## Framework

En programación, un [framework][ref1] es un marco de trabajo que tiene como objetivo facilitar la solución de problemas que pueden surgir al programar. 
Los frameworks nos ayudan acelerando el proceso de programación facilitando tareas como la organización del código o el trabajo en equipo dentro de un proyecto.

También hay que destacar que muchos de estos frameworks no sólo facilitan la organización del trabajo, sino que también ofrecen recursos desarrollados por otros programadores.

Practicamente existen Frameworks para todos los lenguajes de programación.

## Django

[Django][ref2] es un framework web para Python  que fomenta un desarrollo rápido, diseño limpio y pragmático.
Creado por experimentados desarrolladores, quitando carga al desarrollo web, y haciendo que solo se enfoque en las aplicaciones necesarias, sin inventar el hilo negro.

Las principales caracteristicas son:

  - Rápido
  - Seguro
  - Versátil

## MVC - MVT

***Modelo Vista Controlador*** 

- __Modelo__: Es el componente central de esta arquitectura y administra los datos, la lógica y otras restricciones de la aplicación.
- __Vista__: Se ocupa de cómo se mostrarán los datos al usuario y proporciona varios componentes de representación de datos
- __Controlador__: Manipula el modelo y representa la vista actuando como un puente entre ambos

***Modelo Vista Template***

- __Modelo__: Similar a MVC actúa como una interfaz para sus datos y es básicamente la estructura lógica detrás de toda la aplicación web que está representada por una base de datos como MySql, PostgreSQL
  
- __Vista__: Ejecuta la lógica de negocio e interactúa con el Modelo así como la presentación en las plantillas. Acepta solicitudes HTTP y luego devuelve respuestas HTTP
  
- __Template__: Es el componente que diferencia a MVT de MVC. Las plantillas actúan como la capa de presentación y son básicamente código HTML que muestra los datos. El contenido de estos archivos puede ser estáticos o dinámicos

### Direrencias entre [MVC-MVT][ref3]
![Diferencia](assets/MVC-MVT.png)

## Antes de comenzar

Django es un framework para desarrollo con el lenguaje Python, por lo tanto requerimos tener instalada en este caso Python 3.x

  - Python [3.x][ref4]
  - [Pip][ref5] Gestor de Paquetes de Python
  - Instalar con Pip [virtualenv][ref7], para manejo de entornos virtuales
  - Necesitaremos una terminal para la versión básica, en Linux o Mac cualquiera está bien, en caso de windows, se recomienda [git-bash][ref8], en mi caso estaré usando [Tilix][tilix]

Python y Pip es multiplataforma, se puede instalar en cualquier Sistema Operativo, en este Tutorial, trabajaremos en [Arch Linux][arch-linux].

---
[ref1]: https://assemblerinstitute.com/blog/framework-programacion/#:~:text=En%20programaci%C3%B3n%2C%20un%20framework%20es,de%20un%20proyecto%2C%20por%20ejemplo.
[ref2]: https://www.djangoproject.com/
[ref3]: https://www.geeksforgeeks.org/difference-between-mvc-and-mvt-design-patterns/
[ref4]: https://www.python.org/downloads/
[ref5]: https://pypi.org/
[arch-linux]: https://archlinux.org/
[ref7]: https://virtualenv.pypa.io/en/latest/
[ref8]: https://git-scm.com/download/win
[tilix]: https://gnunn1.github.io/tilix-web/